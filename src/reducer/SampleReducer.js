import { UPDATE_SAMPLE_ACTION } from "../actions/types";

const INITIAL_STATE = "Abhishek";

export default function(state = INITIAL_STATE, action) {
    switch (action.type) {
        case UPDATE_SAMPLE_ACTION:
            return action.payload;
        default:
            return state;
    }
}
