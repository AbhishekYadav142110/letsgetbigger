import {
    UPDATE_SAMPLE_ACTION
} from "./types";


export function login(data) {
    return {
        type: UPDATE_SAMPLE_ACTION,
        payload: data
    };
}