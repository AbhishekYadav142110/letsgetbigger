const express = require('express')
const path = require('path')
var bodyParser = require('body-parser')
const MongoClient = require('mongodb').MongoClient
const apis = require('./apis/index')

const DIST_DIR = path.join(__dirname, "../dist")
console.log(DIST_DIR)
console.log("--------------------------------------")

const app = express();

app.use(bodyParser.json())

app.use(express.static(DIST_DIR));

const HTML_FILE = path.join(DIST_DIR, 'index.html')

const uri = "mongodb+srv://AbhishekYadav142110:r3GRXL1MtHFMWrsW@cluster0-ou0x4.mongodb.net/lets_get_bigger?retryWrites=true&w=majority";
// const client = new MongoClient(uri, { useNewUrlParser: true });
// client.connect(err => {
//   if(err){
//     console.log("---------------MongoDb connection error----------------")
//     console.log(err)
//   }
//   console.log("----------------MongoDb connected-------------------")
//   const db = client.db("lets_get_bigger")
//   app.use('/signup', require('./signup')(db));
//   // perform actions on the collection object
//   //client.close();
// });

app.use('/api', apis)

app.get('/*', (req, res) => {
  res.sendFile(HTML_FILE)
})

// // Initialize connection once
// MongoClient.connect(uri, function(err, database) {
//   if(err) throw err;

//   app.locals.db = database;

//   // Start the application after the database connection is ready
//   app.listen(7000, console.log(`Server started on port 7000`));
// });

const client = new MongoClient(uri, { useNewUrlParser: true });
client.connect(err => {
  if(err){
    console.log("---------------MongoDb connection error----------------")
    console.log(err)
  }
  console.log("----------------MongoDb connected-------------------")
  const db = client.db("lets_get_bigger")
  app.locals.db = db
  app.listen(7000, console.log(`Server started on port 7000`));
});


